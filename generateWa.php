<?php
require "function.php";
render_header("Generate Whatsapp");
?>

<?php
if(isset($_POST['btnSubmit'])){
    $text = $_POST['txt_text'];
    $phone = $_POST['txt_phone'];
    $parsePhone = substr_replace($phone, '62', 0, 1);
    

    echo "<script>window.location='https://api.whatsapp.com/send?phone=$parsePhone&text=$text'</script>";
}
?>

<!-- content here -->
<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">WhatsApp Generate Chat</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                        <!-- <a class="dropdown-item" href="#" onclick="document.getElementById('myInput').value = ''">Clear All Text</a> -->
                        <a class="dropdown-item" href="" onclick="clearText()">Clear All Text</a>
                        <script>
                            // function untuk clear text
                            function clearText(){
                                document.getElementById('myInput1').value = ''
                                document.getElementById('myInput2').value = ''
                            }
                        </script>
                        <!-- <a class="dropdown-item" href="#" onclick="document.querySelectorAll('#myInput').values = '' ">Clear All Text</a> -->
                    </div>
                </div>
            </div>

            <!-- Card Body -->
            <div class="card-body">
                <!-- codingan body disini -->
                <form class="user" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <textarea style="text-transform: capitalize;" class="form-control" id="myInput1" rows="3" placeholder="Text Chat..." name="txt_text"></textarea >
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control" id="myInput2" name="txt_phone" placeholder="082321566306" maxlength="13" autocomplete="off" >
                            <!-- <input type="text" class="form-control form-control" id="myInput2" name="txt_phone" placeholder="" maxlength="13" autocomplete="off" onfocus="this.value=''" value="082321566306"> -->
                            
                        </div>
                    </div>

                    <div class="form-group row" >
                        <div class="col-sm-6 mb-3 mb-sm-0" >
                            <button type="submit" name="btnSubmit" class="btn btn-success btn-icon-split">
                                
                                <span class="icon text-white-50">
                                
                                <!-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z"/></svg> -->
                                <i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>
                                </span>
                                <span class="text">Preview WhatsApp</span>
                            </button>
                        </div>
                        
                    </div>

                                
                                
                                
                            </form>
                <!-- codingan body disini -->
            </div>
        </div>
    </div>

    
</div>

</div>
<!-- /.container-fluid -->

<?php
require "include/footer.php";
?>