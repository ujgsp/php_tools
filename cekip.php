<?php
require "function.php";
render_header("Informasi IP");
?>
<!-- content here -->


<div class="row">

    <!-- Border Left Utilities -->
    <div class="col-lg-12">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Informasi IP Anda</h6>
        </div>
        <div class="card-body">
           <!-- table -->
           <div class="table-responsive">
            <table class="table table-bordered">
                <style>
                    th{
                        width: 150px;
                    }
                </style>
                <tbody>
                    <tr>
                        <th scope="row">User IP Address</th>
                        <td><?php echo $_SERVER['REMOTE_ADDR'];?></td>
                    </tr>
                    <tr>
                        <th scope="row">User Agent</th>
                        <td>
                            <?php 
                            // echo $_SERVER['HTTP_USER_AGENT'];
                            $arr_browsers = ["Opera", "Edg", "Chrome", "Safari", "Firefox", "MSIE", "Trident"];
 
                            $agent = $_SERVER['HTTP_USER_AGENT'];
                            
                            $user_browser = '';
                            foreach ($arr_browsers as $browser) {
                                if (strpos($agent, $browser) !== false) {
                                    $user_browser = $browser;
                                    break;
                                }   
                            }
                            switch ($user_browser) {
                                case 'MSIE':
                                    $user_browser = 'Internet Explorer';
                                    break;
                            
                                case 'Trident':
                                    $user_browser = 'Internet Explorer';
                                    break;
                            
                                case 'Edg':
                                    $user_browser = 'Microsoft Edge';
                                    break;
                            }
                            echo $user_browser." browser";
                            ?>
                        </td>
                    </tr>
                </tbody>
                </table>
           </div>
        </div>
    </div>

        <!-- <div class="card mb-4 py-3 border-left-info">
            <div class="card-body">
                .border-left-info
            </div>
        </div> -->

        

    </div>

                      
</div>
<!-- end div footer-->
</div> 
<!-- /.container-fluid -->

<?php
require "include/footer.php";
?>